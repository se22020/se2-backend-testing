﻿using System;
using System.Linq.Expressions;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Diagnostics;
using NUnit.Framework;
using DatabaseApi;

namespace se2_backend_testing {
	[TestFixture(typeof(Bicycle))]
	class LambdaTestsBicycle<T> {
		[Test]
		[TestCase("?construction=TIG Welded", "Construction", "tig welded")]
		[TestCase("?paintid=8", "paintid", "8")]

		/// <summary>
		/// Test the lambda builder to ensure it's returning the expected expression to be used in filtering
		/// </summary>
		/// <param name="qString">Query string to be tested</param>
		/// <param name="propName">The property name being used to compare</param>
		/// <param name="value">The value used to get the string(numeric values still passed as string)</param>

		public static void Builder_Equals_Test(String qString, String propName, String value) {

			var parameters = Expression.Parameter(typeof(T), typeof(T).Name);
			MemberExpression prop = Expression.Property(parameters, propName);
			ConstantExpression cons;
			if(int.TryParse(value, out int parseTry)) {
				cons = Expression.Constant(parseTry);
			} else {
				cons = Expression.Constant(value);
			}
			Expression testExpression = Expression.And(Expression.Not(Expression.Default(typeof(bool))), Expression.Equal(prop, Expression.Convert(cons, prop.Type)));
			Expression<Func<T, bool>> expressionConvert = Expression.Lambda<Func<T, bool>>(testExpression, parameters);
			Expression<Func<T, bool>> retExpression = LambdaBuilder<T>.Builder(qString);
			Assert.AreEqual(expressionConvert.ToString(), retExpression.ToString());
		}
	}


	[TestFixture(typeof(Customer))]
	class LambdaTestsCustomer<T> {
		[Test]
		[TestCase("?firstname=Michael", "firstname", "michael")]
		/// <summary>
		/// Test the lambda builder to ensure it's returning the expected expression to be used in filtering
		/// </summary>
		/// <param name="qString">Query string to be tested</param>
		/// <param name="propName">The property name being used to compare</param>
		/// <param name="value">The value used to get the string(numeric values still passed as string)</param>

		public static void Builder_Equals_Test(String qString, String propName, String value) {

			var parameters = Expression.Parameter(typeof(T), typeof(T).Name);
			MemberExpression prop = Expression.Property(parameters, propName);
			ConstantExpression cons;
			if(int.TryParse(value, out int parseTry)) {
				cons = Expression.Constant(parseTry);
			} else {
				cons = Expression.Constant(value);
			}
			Expression testExpression = Expression.And(Expression.Not(Expression.Default(typeof(bool))), Expression.Equal(prop, Expression.Convert(cons, prop.Type)));
			Expression<Func<T, bool>> expressionConvert = Expression.Lambda<Func<T, bool>>(testExpression, parameters);
			Expression<Func<T, bool>> retExpression = LambdaBuilder<T>.Builder(qString);
			Assert.AreEqual(expressionConvert.ToString(), retExpression.ToString());
		}
	}
}
