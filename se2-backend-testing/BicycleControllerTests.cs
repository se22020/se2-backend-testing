﻿using System;
using System.Threading.Tasks;
using DatabaseApi;
using DatabaseApi.Controllers;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using DatabaseApi.Helpers;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using AutoMapper;
using DatabaseApi.Dtos;
using Microsoft.AspNetCore.Http;


namespace se2_backend_testing {
	/// <summary>
	/// Bicycle Controller Testing Class
	/// </summary>
	[TestFixture]
	class BicycleControllerTests {

		/// <summary>
		/// Test controller to be used for tests
		/// </summary>
		private BicyclesController test_Controller;

		/// <summary>
		/// Creates a config for IMapper
		/// </summary>
		protected static MapperConfiguration config = new MapperConfiguration(conf => {
			conf.ForAllMaps((obj, cfg) => cfg.ForAllMembers(options => options.Condition((source, dest, sourceMember) => sourceMember != null)));
			conf.AddProfile(new AutoMapperProfiles());
		});

		/// <summary>
		/// Set ups a controller between each test
		/// </summary>
		[SetUp]
		public void SetUp() {
			test_Controller = MakeMockController();
		}


		/// <summary>
		/// Test the creation of a context instance.
		/// </summary>
		[Test]
		public void Test_ContextCreation() {
			var mockContext = CreateContext();
			Assert.True(mockContext.Bicycle.Any());
		}

		/// <summary>
		/// Test GetAll in Bicycle Controller
		/// </summary>
		/// <returns>Task based on the test</returns>
		[Test]
		[Category("GetAll")]
		public async Task Test_GetAll() {
			UserParams up = new UserParams();
			up.PageSize = 10;
			up.PageNumber = 1;
			var actionResult = await test_Controller.GetAll(up);
			Assert.IsInstanceOf(typeof(OkObjectResult), actionResult.Result);
		}

		/// <summary>
		/// Test the HTTP GET method returning a bicycle by ID.
		/// </summary>
		/// <param name="expectedID">The SerialNumber of the bicycle wished to be returned.</param>
		/// <returns>A task produced by the GET</returns>
		[Test]
		[Category("GetBy")]
		[TestCase(10)]
		public async Task Test_GetByID(int expectedID) {
			var bicycle =(OkObjectResult)await test_Controller.GetBicycle(expectedID);
			Assert.AreEqual(typeof(OkObjectResult), bicycle.GetType());
		}

		/// <summary>
		/// Test to GetByID for Bicycle Class
		/// </summary>
		/// <param name="expectedID"></param>
		/// <returns>A task based on the request</returns>
		[Test]
		[Category("GetBy")]
		[TestCase(9999999)]
		public async Task Test_GetByID_Fail(int expectedID) {
			var actionResult = await test_Controller.GetBicycle(expectedID);
			Assert.IsInstanceOf(typeof(NotFoundResult), actionResult);
		}


		/// <summary>
		/// Test UpdateBicycle in the Bicycle Controller
		/// </summary>
		/// <param name="serialNumber">The serialnumber to be updated</param>
		/// <returns>A task based on the request</returns>
		[Test]
		[Category("Update")]
		[TestCase(10)]
		public async Task Test_UpdateBicycle(int serialNumber) {
			BicycleToUpdate updateTo = new BicycleToUpdate();
			updateTo.Frameprice = 9001;
			var actionResult = (OkObjectResult)await test_Controller.UpdateBicycle(serialNumber, updateTo);
			Assert.IsInstanceOf(typeof(OkObjectResult), actionResult);
		}

		/// <summary>
		/// Test CreateBicycle in the BicycleController
		/// </summary>
		/// <returns>A task based on the request</returns>
		[Test]
		[Category("Create")]
        public async Task Test_CreateBicycle() {
			BicycleToCreate create = new BicycleToCreate();
			create.Customname = "CreateTestPass";
			create.Construction = "TIG Welded";
			create.Customerid = 1;
			create.Paintid = 1;
			var actionResult = (OkObjectResult)await test_Controller.CreateBicycle(create);
			Assert.IsInstanceOf(typeof(OkObjectResult), actionResult);
		}

		/// <summary>
		/// Test CreateBicycle(Expected to fail) from BicycleController
		/// </summary>
		/// <returns>A task  based on the request</returns>
		[Test]
		[Category("Create")]
		public async Task Test_CreateBicycle_Fail() {
			BicycleToCreate create = new BicycleToCreate();
			create.Customname = "CreateTestPass";
			create.Construction = "TIG Welded";
			create.Customerid = 1;
			create.Paintid = 1;
			test_Controller.ModelState.AddModelError("SerialNumber", "SerialNumber Is Mandatory");
			var actionResult = (BadRequestResult)await test_Controller.CreateBicycle(create);
			Assert.IsInstanceOf(typeof(BadRequestResult), actionResult);
		}

		/// <summary>
		/// Test DeleteBicycle in the BicycleController
		/// </summary>
		/// <param name="serialNumber">The serialnumber to be deleted</param>
		/// <returns>A task based on the request</returns>
		[Test]
		[Category("Delete")]
		[TestCase(4)]
		public async Task Test_DeleteBicycle(int serialNumber) {
			var actionResult = await test_Controller.DeleteBicycle(serialNumber);
            if (actionResult.GetType() == typeof(NotFoundResult))
                Assert.IsInstanceOf(typeof(NotFoundResult), actionResult);
			else
                Assert.IsInstanceOf(typeof(OkObjectResult), actionResult);
		}

		/// <summary>
		/// Test DeleteBicycle(expected to fail) in BicycleController
		/// </summary>
		/// <param name="serialNumber">SerialNumber to be attampted to be deleted</param>
		/// <returns>A task based on the request</returns>
		[Test]
		[Category("Delete")]
		[TestCase(-1)]
		public async Task Test_DeleteBicycle_Fail(int serialNumber) {
			var actionResult = (NotFoundResult)await test_Controller.DeleteBicycle(serialNumber);
			Assert.IsInstanceOf(typeof(NotFoundResult), actionResult);
		}

		/// <summary>
		/// Creates the context for the controller
		/// </summary>
		/// <returns>A BikeShop_Context</returns>
		private BikeShop_Context CreateContext() {
			var options = new DbContextOptionsBuilder<BikeShop_Context>().UseMySQL("server=71.87.195.218;user=BIKE_SHOP;database=BIKE_SHOP;password=TeamADatabase1!;Convert Zero Datetime=true;").Options;
			var mockContext = new BikeShop_Context(options);
			mockContext.Database.EnsureCreated();
			return mockContext;
		}


		/// <summary>
		/// Creates a controller to use to test
		/// </summary>
		/// <returns>The test controller</returns>
		private BicyclesController MakeMockController() {
			IMapper imap = new Mapper(config);
			var mockContext = CreateContext();
			var mockMonitoringService = new MonitoringService("https://metricsapi20201007030533.azurewebsites.net");
			BicyclesController test_Controller = new BicyclesController(mockContext, imap, mockMonitoringService);
			test_Controller.ControllerContext = new ControllerContext();
			test_Controller.ControllerContext.HttpContext = new DefaultHttpContext();
			test_Controller.Request.QueryString = new QueryString("?PageSize=10&PageNumber=1");
			return test_Controller;
		}
	}
}
