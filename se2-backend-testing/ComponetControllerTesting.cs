﻿using AutoMapper;
using DatabaseApi;
using DatabaseApi.Controllers;
using DatabaseApi.Dtos;
using DatabaseApi.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace se2_backend_testing
{
    [TestFixture]
    class ComponetControllerTesting
    {
        protected static MapperConfiguration config = new MapperConfiguration(conf => {
            conf.ForAllMaps((obj, cfg) => cfg.ForAllMembers(options => options.Condition((source, dest, sourceMember) => sourceMember != null)));
            conf.AddProfile(new AutoMapperProfiles());
        });

        protected static IMapper mapper = config.CreateMapper();
        public BikeShop_Context CreateContext()
        {
            var options = new DbContextOptionsBuilder<BikeShop_Context>().UseMySQL("server=71.87.195.218;user=BIKE_SHOP;database=BIKE_SHOP;password=TeamADatabase1!;Convert Zero Datetime=true;").Options;
            var mockContext = new BikeShop_Context(options);
            mockContext.Database.EnsureCreated();

            return mockContext;
        }
        /// <summary>
        /// GetAll Pass, can not fail, no user input.
        /// </summary>
        /// <returns></returns>
        [Test]
        [Category("GetAll")]
        public async Task GetAll() 
        {
            var mockContext = CreateContext();
            var mockMonitoringService = new MonitoringService("https://metricsapi20201007030533.azurewebsites.net");
            ComponentController test_Controller = new ComponentController(mockContext, mapper, mockMonitoringService);
            test_Controller.ControllerContext = new ControllerContext();
            test_Controller.ControllerContext.HttpContext = new DefaultHttpContext();
            test_Controller.Request.QueryString = new QueryString("?PageSize=10&PageNumber=1");
            UserParams user = new UserParams();
            var actionResult = await test_Controller.GetAll(user);
            Assert.IsInstanceOf(typeof(OkObjectResult), actionResult.Result);

        }
        /// <summary>
        /// Get By ID Pass
        /// </summary>
        /// <param name="expectedID"></param>
        /// <returns></returns>
        [Test]
        [Category("GetByComponent")]
        [TestCase(1)]
        public async Task GetByID(int expectedID) 
        {
            var mockContext = CreateContext();
            var mockMonitoringService = new MonitoringService("https://metricsapi20201007030533.azurewebsites.net");
            ComponentController test_Controller = new ComponentController(mockContext, mapper, mockMonitoringService);
            var component = (OkObjectResult)await test_Controller.GetComponent(expectedID);
            Assert.AreEqual(expectedID, ((Component)component.Value).Componentid);
        }
        /// <summary>
        /// Get By ID Fail
        /// </summary>
        /// <param name="expectedID"></param>
        /// <returns></returns>
        [Test]
        [Category("GetByComponent_Fail")]
        [TestCase(999999)]
        public async Task GetByID_Fail(int expectedID)
        {
            var mockContext = CreateContext();
            var mockMonitoringService = new MonitoringService("https://metricsapi20201007030533.azurewebsites.net");
            ComponentController test_Controller = new ComponentController(mockContext, mapper, mockMonitoringService);
            var component = (NotFoundResult)await test_Controller.GetComponent(expectedID);
            Assert.IsInstanceOf(typeof(NotFoundResult), component);
        }
        /// <summary>
        /// Update Pass
        /// </summary>
        /// <param name="componentID"></param>
        /// <returns></returns>
        [Test]
        [Category("Update")]
        [TestCase(1)]
        public async Task Test_UpdateComponent(int componentID)
        {
            ComponetToUpdate updateTo = new ComponetToUpdate();
            IMapper imap = new Mapper(config);
            var mockContext = CreateContext();
            updateTo.Road = "Test";
            var mockMonitoringService = new MonitoringService("https://metricsapi20201007030533.azurewebsites.net");
            ComponentController test_Controller = new ComponentController(mockContext, imap, mockMonitoringService);
            var actionResult = (OkObjectResult)await test_Controller.UpdateComponent(componentID, updateTo);
            Assert.IsInstanceOf(typeof(OkObjectResult), actionResult);
        }
        /// <summary>
        /// Update Fail
        /// </summary>
        /// <param name="componentID"></param>
        /// <returns></returns>
        [Test]
        [Category("Update_Fail")]
        [TestCase(999999)]
        public async Task Test_UpdateComponent_Fail(int componentID)
        {
            ComponetToUpdate updateTo = new ComponetToUpdate();
            IMapper imap = new Mapper(config);
            var mockContext = CreateContext();
            updateTo.Road = "Test";
            var mockMonitoringService = new MonitoringService("https://metricsapi20201007030533.azurewebsites.net");
            ComponentController test_Controller = new ComponentController(mockContext, imap, mockMonitoringService);
            var actionResult = (NoContentResult)await test_Controller.UpdateComponent(componentID, updateTo);
            Assert.IsInstanceOf(typeof(NoContentResult), actionResult);
        }

        /// <summary>
        /// Delete Pass, make sure you have Component with ID of 9999999 in DB when exicuted.
        /// </summary>
        /// <param name="componentID"></param>
        /// <returns></returns>
        [Test]
        [Category("Delete")]
        [TestCase(320)]
        public async Task Test_Delete(int componentID)
        {
            IMapper imap = new Mapper(config);
            var mockContext = CreateContext();

            var mockMonitoringService = new MonitoringService("https://metricsapi20201007030533.azurewebsites.net");
            ComponentController test_Controller = new ComponentController(mockContext, imap, mockMonitoringService);
            var actionResult =(OkObjectResult) await test_Controller.DeleteComponent(componentID);
            Assert.IsInstanceOf(typeof(OkObjectResult),actionResult);
        }
        /// <summary>
        /// Delete Fail
        /// </summary>
        /// <param name="componentID"></param>
        /// <returns></returns>
        [Test]
        [Category("Delete_Fail")]
        [TestCase(-9999999)]
        public async Task Test_Delete_Fail(int componentID)
        {
            IMapper imap = new Mapper(config);
            var mockContext = CreateContext();
            var mockMonitoringService = new MonitoringService("https://metricsapi20201007030533.azurewebsites.net");
            ComponentController test_Controller = new ComponentController(mockContext, imap, mockMonitoringService);
            var actionResult = (NotFoundResult)await test_Controller.DeleteComponent(componentID);
            Assert.IsInstanceOf(typeof(NotFoundResult), actionResult);
        }


        /// <summary>
        /// Create Pass
        /// </summary>
        /// <returns></returns>
        [Test]
        [Category("Create")]
        public async Task Test_CreateComponent() {
            ComponetToCreate createTo = new ComponetToCreate();
            createTo.Road = "Test";
            createTo.Manufacturerid = 33;
            createTo.Componentid = 230;
            createTo.Category = "Frame";
            IMapper imap = new Mapper(config);
            var mockContext = CreateContext();
            var mockMonitoringService = new MonitoringService("https://metricsapi20201007030533.azurewebsites.net");
            ComponentController test_Controller = new ComponentController(mockContext, imap, mockMonitoringService);
            var actionResult = await test_Controller.CreateComponent(createTo);
            Assert.IsInstanceOf(typeof(OkObjectResult), actionResult);

        }
        /// <summary>
        /// Create Fail
        /// </summary>
        /// <returns></returns>
        [Test]
        [Category("Create_Fail")]
        public async Task Test_CreateComponent_Fail() {
            ComponetToCreate createTo = new ComponetToCreate();
            createTo.Road = "Test";
            createTo.Manufacturerid = 33;
            createTo.Category = "Frame";
            IMapper imap = new Mapper(config);
            var mockContext = CreateContext();
            var mockMonitoringService = new MonitoringService("https://metricsapi20201007030533.azurewebsites.net");
            ComponentController test_Controller = new ComponentController(mockContext, imap, mockMonitoringService);
            test_Controller.ModelState.AddModelError("CustomerID", "CustomerID Is Mandatory");
            var actionResult = await test_Controller.CreateComponent(createTo);
            Assert.IsInstanceOf(typeof(BadRequestResult), actionResult);

        }


    }
}
