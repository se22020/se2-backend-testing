﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DatabaseApi;
using DatabaseApi.Controllers;
using DatabaseApi.Models;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using DatabaseApi.Helpers;
using System.Web.Http;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using AutoMapper;
using DatabaseApi.Dtos;
using Microsoft.AspNetCore.Http;

namespace se2_backend_testing
{
    [TestFixture]
    class CustomerControllerTests
    {

       protected static MapperConfiguration config = new MapperConfiguration(conf => {
        conf.ForAllMaps((obj, cfg) => cfg.ForAllMembers(options => options.Condition((source, dest, sourceMember) => sourceMember != null)));
        conf.AddProfile(new AutoMapperProfiles());
       });

        protected static IMapper mapper = config.CreateMapper();
        public BikeShop_Context CreateContext()
        {
            var options = new DbContextOptionsBuilder<BikeShop_Context>().UseMySQL("server=71.87.195.218;user=BIKE_SHOP;database=BIKE_SHOP;password=TeamADatabase1!;Convert Zero Datetime=true;").Options;
            var mockContext = new BikeShop_Context(options);
            mockContext.Database.EnsureCreated();

            return mockContext;
        }
        /// <summary>
        /// Get all by CityID Pass
        /// </summary>
        /// <param name="expectedID"></param>
        /// <returns></returns>
        [Test]
        [Category("GetByCityID")]
        [TestCase(3348)]
        public async Task Test_GetByCityID(int expectedID)
        {
            var mockContext = CreateContext();
            var mockMonitoringService = new MonitoringService("https://metricsapi20201007030533.azurewebsites.net");
            CustomerController test_Controller = new CustomerController(mockContext, mapper, mockMonitoringService);
            var customer = await test_Controller.GetByCityId(expectedID);
            Assert.IsInstanceOf(typeof(OkObjectResult), customer);
        }
        /// <summary>
        /// Get all by CityID Fail
        /// </summary>
        /// <param name="expectedID"></param>
        /// <returns></returns>
        [Test]
        [Category("GetByCityID_Fail")]
        [TestCase(9999999)]
        public async Task Test_GetByCityID_Fail(int expectedID)
        {
            var mockContext = CreateContext();
            var mockMonitoringService = new MonitoringService("https://metricsapi20201007030533.azurewebsites.net");
            CustomerController test_Controller = new CustomerController(mockContext, mapper, mockMonitoringService);
            var customer = await test_Controller.GetByCityId(expectedID);
            Assert.IsInstanceOf(typeof(NoContentResult), customer);
        }
        /// <summary>
        /// Get all by ZipCode Pass
        /// </summary>
        /// <param name="expectedID"></param>
        /// <returns></returns>
        [Test]
        [Category("GetByZipCode")]
        [TestCase("99501")]
        public async Task Test_GetByZipCode(string expectedID)
        {
            var mockContext = CreateContext();
            var mockMonitoringService = new MonitoringService("https://metricsapi20201007030533.azurewebsites.net");
            CustomerController test_Controller = new CustomerController(mockContext, mapper, mockMonitoringService);
            var customer = await test_Controller.GetByZipcode(expectedID);
            Assert.IsInstanceOf(typeof(OkObjectResult), customer);
        }
        /// <summary>
        /// Get all by ZipCode Fail
        /// </summary>
        /// <param name="expectedID"></param>
        /// <returns></returns>
        [Test]
        [Category("GetByZipCode_Fail")]
        [TestCase("This should fail")]
        public async Task Test_GetByZipCode_Fail(string expectedID)
        {
            var mockContext = CreateContext();
            var mockMonitoringService = new MonitoringService("https://metricsapi20201007030533.azurewebsites.net");
            CustomerController test_Controller = new CustomerController(mockContext, mapper, mockMonitoringService);
            var customer = await test_Controller.GetByZipcode(expectedID);
            Assert.IsInstanceOf(typeof(NoContentResult), customer);
        }
        /// <summary>
        /// Get by ID Pass
        /// </summary>
        /// <param name="expectedID"></param>
        /// <returns></returns>
        [Test]
        [Category("GetByID")]
        [TestCase(1)]
        public async Task Test_GetByID(int expectedID)
        {
            var mockContext = CreateContext();
            var mockMonitoringService = new MonitoringService("https://metricsapi20201007030533.azurewebsites.net");
            CustomerController test_Controller = new CustomerController(mockContext, mapper, mockMonitoringService);
            var customer = await test_Controller.GetById(expectedID);
            Assert.IsInstanceOf(typeof(OkObjectResult), customer);
        }
        /// <summary>
        /// Get by ID Fail
        /// </summary>
        /// <param name="expectedID"></param>
        /// <returns></returns>
        [Test]
        [Category("GetByID_Fail")]
        [TestCase(9999999)]
        public async Task Test_GetByID_Fail(int expectedID)
        {
            var mockContext = CreateContext();
            var mockMonitoringService = new MonitoringService("https://metricsapi20201007030533.azurewebsites.net");
            CustomerController test_Controller = new CustomerController(mockContext, mapper, mockMonitoringService);
            var customer = await test_Controller.GetById(expectedID);
            Assert.IsInstanceOf(typeof(NoContentResult), customer);
        }
        /// <summary>
        /// Update Customer Pass
        /// </summary>
        /// <param name="customerID"></param>
        /// <returns></returns>
        [Test]
        [Category("Update")]
        [TestCase(1)]
        public async Task Test_UpdateCustomer(int customerID)
        {
            CustomerToUpdate updateTo = new CustomerToUpdate();
            IMapper imap = new Mapper(config);
            var mockContext = CreateContext();
            updateTo.Lastname = "Test";
            var mockMonitoringService = new MonitoringService("https://metricsapi20201007030533.azurewebsites.net");
            CustomerController test_Controller = new CustomerController(mockContext, imap, mockMonitoringService);
            var actionResult = (OkObjectResult)await test_Controller.UpdateCustomer(customerID, updateTo);
            Assert.IsInstanceOf(typeof(OkObjectResult), actionResult);
        }
        /// <summary>
        /// Update Customer Fail
        /// </summary>
        /// <param name="customerID"></param>
        /// <returns></returns>
        [Test]
        [Category("Update_Fail")]
        [TestCase(-1)]
        public async Task Test_UpdateCustomer_Fail(int customerID)
        {
            CustomerToUpdate updateTo = new CustomerToUpdate();
            IMapper imap = new Mapper(config);
            var mockContext = CreateContext();
            updateTo.Lastname = "Test";
            var mockMonitoringService = new MonitoringService("https://metricsapi20201007030533.azurewebsites.net");
            CustomerController test_Controller = new CustomerController(mockContext, imap, mockMonitoringService);
            var actionResult = (NoContentResult)await test_Controller.UpdateCustomer(customerID, updateTo);
            Assert.IsInstanceOf(typeof(NoContentResult), actionResult);
        }
        /// <summary>
        /// Make sure the customer record with ID 999,999 is in the database before running.
        /// </summary>
        /// <param name="customerID"></param>
        /// <returns></returns>
        [Test]
        [Category("Delete")]
        [TestCase(23248)]
        public async Task Test_Delete(int customerID) 
        {
            IMapper imap = new Mapper(config);
            var mockContext = CreateContext();
            var mockMonitoringService = new MonitoringService("https://metricsapi20201007030533.azurewebsites.net");
            CustomerController test_Controller = new CustomerController(mockContext, imap, mockMonitoringService);
            var actionResult = await test_Controller.DeleteCustomer(customerID);
            Assert.IsInstanceOf(typeof(OkObjectResult), actionResult);
        }
        /// <summary>
        /// Delete Customer Fail
        /// </summary>
        /// <param name="customerID"></param>
        /// <returns></returns>
        [Test]
        [Category("Delete_Fail")]
        [TestCase(-1)]
        public async Task Test_DeleteCustomer_Fail(int customerID) 
        {
            IMapper imap = new Mapper(config);
            var mockContext = CreateContext();
            var mockMonitoringService = new MonitoringService("https://metricsapi20201007030533.azurewebsites.net");
            CustomerController test_Controller = new CustomerController(mockContext, imap, mockMonitoringService);
            var actionResult = await test_Controller.DeleteCustomer(customerID);
            Assert.IsInstanceOf(typeof(BadRequestResult), actionResult);
        }
        /// <summary>
        /// Create Customer Pass
        /// </summary>
        /// <returns></returns>
        [Test]
        [Category("Create")]
        public async Task Test_CreateCustomer() 
        {
            CustomerToCreate createTo = new CustomerToCreate {
                Firstname = "Test",
                Lastname = "Test",
                Cityid = 2
            };
            IMapper imap = new Mapper(config);
            var mockContext = CreateContext();
            var mockMonitoringService = new MonitoringService("https://metricsapi20201007030533.azurewebsites.net");
            CustomerController test_Controller = new CustomerController(mockContext, imap, mockMonitoringService);
            var actionResult = await test_Controller.CreateCustomer(createTo);
            Assert.IsInstanceOf(typeof(OkObjectResult), actionResult);

        }
        /// <summary>
        /// Create Customer Fail
        /// </summary>
        /// <returns></returns>
        [Test]
        [Category("Create_Fail")]
        public async Task Test_CreateCustomer_Fail() 
        {
            CustomerToCreate createTo = new CustomerToCreate();
            createTo.Firstname = "Test";
            createTo.Lastname = "Test";
            IMapper imap = new Mapper(config);
            var mockContext = CreateContext();
            var mockMonitoringService = new MonitoringService("https://metricsapi20201007030533.azurewebsites.net");
            CustomerController test_Controller = new CustomerController(mockContext, imap, mockMonitoringService);
            test_Controller.ModelState.AddModelError("CustomerID", "CustomerID Is Mandatory");
            var actionResult = await test_Controller.CreateCustomer(createTo);
            Assert.IsInstanceOf(typeof(BadRequestResult), actionResult);

        }
        /// <summary>
        /// Get All Pass, the getAll method will not break since it does not rely on user input.
        /// </summary>
        /// <returns></returns>
        [Test]
        [Category("GetAll")]
        public async Task Test_GetAll() 
        {
            var mockContext = CreateContext();
            var mockMonitoringService = new MonitoringService("https://metricsapi20201007030533.azurewebsites.net");
            CustomerController test_Controller = new CustomerController(mockContext, mapper, mockMonitoringService);
            test_Controller.ControllerContext = new ControllerContext();
            test_Controller.ControllerContext.HttpContext = new DefaultHttpContext();
            test_Controller.Request.QueryString = new QueryString("?PageSize=10&PageNumber=1");
            UserParams user = new UserParams();
            var actionResult = await test_Controller.GetAll(user);
            Assert.IsInstanceOf(typeof(OkObjectResult), actionResult);
        }

    }
}
