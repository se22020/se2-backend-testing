﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DatabaseApi;
using DatabaseApi.Controllers;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using DatabaseApi.Helpers;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using AutoMapper;
using DatabaseApi.Dtos;
using Microsoft.AspNetCore.Http;


namespace se2_backend_testing {
    /// <summary>
    /// Test class PaintsController.cs
    /// </summary>
    [TestFixture]
    public class PaintsControllerTests {

        /// <summary>
        /// Test controller to be used for tests
        /// </summary>
        private PaintsController test_Controller;

        #region Setup
        /// <summary>
        /// Creates a config for IMapper
        /// </summary>
        protected static MapperConfiguration config = new MapperConfiguration(conf => {
            conf.ForAllMaps((obj, cfg) => cfg.ForAllMembers(options => options.Condition((source, dest, sourceMember) => sourceMember != null)));
            conf.AddProfile(new AutoMapperProfiles());
        });


        /// <summary>
        /// Set ups a controller between each test
        /// </summary>
        [SetUp]
        public void SetUp() {
            test_Controller = MakeMockController();
        }

        /// <summary>
        /// Creates a controller to use to test
        /// </summary>
        /// <returns>The test controller</returns>
        private PaintsController MakeMockController() {
            IMapper imap = new Mapper(config);
            BikeShop_Context mockContext = CreateContext();
            PaintsController controller = new PaintsController(mockContext, imap);
            controller.ControllerContext = new ControllerContext();
            controller.ControllerContext.HttpContext = new DefaultHttpContext();
            controller.Request.QueryString = new QueryString("?PageSize=10&PageNumber=1");
            return controller;
        }

        /// <summary>
        /// Creates the context for the controller
        /// </summary>
        /// <returns>A BikeShop_Context</returns>
        private BikeShop_Context CreateContext() {
            var options = new DbContextOptionsBuilder<BikeShop_Context>().UseMySQL("server=71.87.195.218;user=BIKE_SHOP;database=BIKE_SHOP;password=TeamADatabase1!;Convert Zero Datetime=true;").Options;
            var mockContext = new BikeShop_Context(options);
            mockContext.Database.EnsureCreated();
            return mockContext;
        }

        #endregion

        #region Category: GetByID

        /// <summary>
        /// Test the HTTP GET method returning a paint by ID
        /// </summary>
        /// <param name="expectedID">The PaintID of the paint to return.</param>
        /// <returns>A task produced by the GET</returns>
        [Test]
        [Category("GetByID")]
        [TestCase(8)]
        [TestCase(9)]
        [TestCase(2)]
        public async Task Test_GetByID(int expectedID) {
            var paint = (OkObjectResult)await test_Controller.GetPaint(expectedID);
            Assert.AreEqual(expectedID, ((Paint) paint.Value).Paintid);
        }

        /// <summary>
        /// Test the HTTP GET method returning a paint by ID
        /// </summary>
        /// <param name="expectedID">The PaintID of the paint to return.</param>
        /// <returns>A task produced by the GET</returns>
        [Test]
        [Category("GetByID")]
        [TestCase(-1)]
        [TestCase(9999)]
        public async Task Test_GetByID_Fail(int expectedID) {
            var paint = (NotFoundResult)await test_Controller.GetPaint(expectedID);
            Assert.IsInstanceOf(typeof(NotFoundResult), paint);
        }

        #endregion

        #region Category: GetAll
        /// <summary>
        /// Test GetPaint in PaintsController
        /// </summary>
        /// <returns>Task based on the test</returns>
        [Test]
        [Category("GetAll")]
        public async Task Test_GetPaint() {
            var actionResult = await test_Controller.GetPaint();
            Assert.IsInstanceOf(typeof(OkObjectResult), actionResult.Result);
        }

        #endregion


        /// <summary>
        /// Tests the PutPaint.
        /// </summary>
        /// <param name="paintID">The paint identifier.</param>
        [Test]
        [Category("Update")]
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        public async Task Test_PutPaint(int paintID) {
            PaintToUpdate updateTo = new PaintToUpdate();
            updateTo.Colorname = "Teal";
            var actionResult = (OkObjectResult)await test_Controller.PutPaint(paintID, updateTo);
            Assert.IsInstanceOf(typeof(OkObjectResult), actionResult);
        }

        /// <summary>
        /// Tests the DeletePaint.
        /// </summary>
        /// <param name="paintID">The paint identifier.</param>
        [Test]
        [TestCase(51)]
        public async Task Test_DeletePaint(int paintID) {
            var actionResult = await test_Controller.DeletePaint(paintID);
            Assert.IsInstanceOf(typeof(OkObjectResult), actionResult);
        }


        /// <summary>
        /// Tests the DeletePaint for failure.
        /// </summary>
        /// <param name="paintID">The paint identifier.</param>
        [Test]
        [TestCase(999)]
        public async Task Test_DeletePaint_Fail(int paintID) {
            var actionResult = (NotFoundResult)await test_Controller.DeletePaint(paintID);
            Assert.IsInstanceOf(typeof(NotFoundResult), actionResult);
        }

        /// <summary>
        /// Test PostPaint in the PaintsController
        /// </summary>
        /// <returns>A task based on the request</returns>
        [Test]
        [Category("Create")]
        [TestCase("sampleName",  "BLUE")]
        [TestCase("jimmyColor",  "BLUE")]
        [TestCase("jimmyColor", "BLUE")]
        [TestCase("jimmyColor",  "BLUE")]
        public async Task Test_PostPaint( string clrName, string clrList) {
            var newPaint = new PaintToCreate() {
                Colorname = clrName,
                Colorlist = clrList
            };

            var actionResult = (OkObjectResult)await test_Controller.PostPaint(newPaint);
            Assert.IsInstanceOf(typeof(OkObjectResult), actionResult);
            Assert.That(actionResult.Value, Has.Property("Colorname").EqualTo(clrName)
                                           .And.Property("Colorlist").EqualTo(clrList));
        }
    }
}