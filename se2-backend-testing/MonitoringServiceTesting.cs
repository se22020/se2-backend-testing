﻿using DatabaseApi.Helpers;
using DatabaseApi.MonitoringServiceModels;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace se2_backend_testing
{
    class MonitoringServiceTesting
    {
        /// <summary>
        /// Test if code can update to monitoring service
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        [Test]
        [Category("SendUpdate")]
        [TestCase("https://metricsapi20201108200731.azurewebsites.net")]
        public async Task Test_SendUpdate(string url) 
        {
            MonitoringService monitoring = new MonitoringService(url);
            DatabaseApi.MonitoringServiceModels.Transaction transaction = new DatabaseApi.MonitoringServiceModels.Transaction();
            transaction.time_Stamp = DateTime.Now;
            var test = await monitoring.SendUpdateAsync("api/transaction/post",transaction);
            Assert.AreEqual(true,test); 
        }

        /// <summary>
        /// Test to see if bad request will not be posted
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        [Test]
        [Category("SendUpdate_Fail")]
        [TestCase("https://metricsapi20201108200731.azurewebsites.net/")]
        public async Task Test_SendUpdate_Fail(string url)
        {
            MonitoringService monitoring = new MonitoringService(url);
            DatabaseApi.MonitoringServiceModels.Transaction transaction = new DatabaseApi.MonitoringServiceModels.Transaction();
            transaction.time_Stamp = DateTime.Now;
            var test = await monitoring.SendUpdateAsync("api/transaction/put", transaction);
            Assert.AreEqual(false, test);
        }



    }
}
